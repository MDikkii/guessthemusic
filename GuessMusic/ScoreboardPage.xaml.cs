﻿using GuessMusic.DataService;
using GuessMusic.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace GuessMusic
{
    public partial class ScoreboardPage : PhoneApplicationPage
    {
        public List<HistoryItem> History = new List<HistoryItem>();

        public ScoreboardPage()
        {
            InitializeComponent();
            BuildLocalizedApplicationBar();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            await ScoreHistory.Instance.InitHistory();
            History = ScoreHistory.Instance.GetHistory();
            HistoryList.ItemsSource = History;
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarButton =
                new ApplicationBarIconButton(new
                Uri("/Assets/AppBar/delete.png", UriKind.Relative));
            appBarButton.Text = AppResources.ClearHistoryButton;
            appBarButton.Click += ClearHistory_Click;

            ApplicationBar.Buttons.Add(appBarButton);
            ApplicationBar.BackgroundColor = Colors.White;
            ApplicationBar.ForegroundColor = Colors.Black;
            ApplicationBar.IsMenuEnabled = true;
            ApplicationBar.IsVisible = true;
        }

        private async void ClearHistory_Click(object sender, EventArgs e)
        {
            await ScoreHistory.Instance.ClearHistory();
            History = new List<HistoryItem>();
            HistoryList.ItemsSource = History;
        }
    }
}
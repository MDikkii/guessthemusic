﻿using GuessMusic.DataService;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Nokia.Music;
using Nokia.Music.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace GuessMusic
{
    public partial class GenrePage : PhoneApplicationPage
    {
        public List<GenreHelp> Genres;

        public GenrePage()
        {
            InitializeComponent();
        }

        public async Task Init()
        {
            Genres = await MusicService.Instance.GetGenres();
            GenresList.ItemsSource = Genres;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await Init();
            base.OnNavigatedTo(e);
        }

        private void GenresList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string uriString = "/GamePage.xaml";

            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            if (!settings.Contains("lastGenre"))
            {
                settings.Add("lastGenre", ((GenreHelp)GenresList.SelectedItem).Id);
            }
            else
            {
                settings["lastGenre"] = ((GenreHelp)GenresList.SelectedItem).Id;
            }
            settings.Save();

            NavigationService.Navigate(new Uri(uriString, UriKind.Relative));
        }
    }
}
﻿using GuessMusic.DataService;
using GuessMusic.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Nokia.Music;
using Nokia.Music.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using Windows.Phone.Speech.Recognition;

namespace GuessMusic
{
    public partial class GamePage : PhoneApplicationPage
    {
        private TrackQuestion currentTrack;
        private ListResponse<Product> Songs;
        private List<TrackQuestion> SongsToPlay;

        public GamePage()
        {
            InitializeComponent();
            BuildLocalizedApplicationBar();
            InitSpeechRecognition();
        }

        public int currentQuestionCount { get; set; }

        private int points { get; set; }

        private SpeechRecognizerUI RecoWithUI { get; set; }

        public async Task InitGame()
        {
            try
            {
                string genreName = string.Empty;
                if (IsolatedStorageSettings.ApplicationSettings.Contains("lastGenre"))
                {
                    genreName = IsolatedStorageSettings.ApplicationSettings["lastGenre"] as string;

                    Songs = await MusicService.Instance.mc.GetTopProductsForGenreAsync(genreName, Category.Track, 0, 30);
                    Debug.WriteLine(Songs.Result.First().Name + " " + Songs.Result.First().Performers.First().Name);
                    Debug.WriteLine(Songs.Count);

                    StartGame();
                    AnswerOne.Visibility = Visibility.Visible;
                    AnswerTwo.Visibility = Visibility.Visible;
                    AnswerThree.Visibility = Visibility.Visible;
                    displayText.Visibility = Visibility.Collapsed;
                }
                else
                {
                    NavigationService.GoBack();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(AppResources.LackOfInternet);
            }
        }

        public List<TrackQuestion> InitSongsToPlayList()
        {
            List<TrackQuestion> temp = new List<TrackQuestion>();
            Random r = new Random();
            Product song;
            string goodAnswer = string.Empty;
            Product badAnswerOne = null;
            Product badAnswerTwo = null;
            string[] badAnswers;
            int indexRandom;
            int indexRandomSec;
            int songIndex;
            for (int i = 0; i < 10; i++)
            {
                songIndex = r.Next(0, Songs.Count);
                song = Songs.ElementAt(songIndex);
                goodAnswer = song.Name + "\n" + song.Performers[0].Name;
                Songs.RemoveAt(songIndex);

                indexRandom = r.Next(0, Songs.Count);
                badAnswerOne = Songs.ElementAt(indexRandom);

                indexRandomSec = indexRandom;
                while (indexRandomSec == indexRandom)
                {
                    indexRandomSec = r.Next(0, Songs.Count);
                }
                badAnswerTwo = Songs.ElementAt(indexRandomSec);
                badAnswers = new string[] { badAnswerOne.Name + "\n" + badAnswerOne.Performers[0].Name, badAnswerTwo.Name + "\n" + badAnswerTwo.Performers[0].Name };

                temp.Add(new TrackQuestion(song.Name, song.Id, song.Performers[0].Name, goodAnswer, badAnswers));
            }
            return temp;
        }

        public void InitSpeechRecognition()
        {
            // Create an instance of SpeechRecognizerUI.
            RecoWithUI = new SpeechRecognizerUI();

            //workaround speachrecognition error not supportet language
            var localRec = InstalledSpeechRecognizers.All.Where(r => r.Language == CultureInfo.CurrentUICulture.Name).FirstOrDefault();
            if (localRec == null)
            {
                localRec = InstalledSpeechRecognizers.All.FirstOrDefault();
            }
            if (localRec != null)
            {
                RecoWithUI.Recognizer.SetRecognizer(localRec);
            }
            else
            {
                ApplicationBar.IsVisible = false;
                MessageBox.Show("Notsupported speechrecognition");
            }
        }

        public void NextTrack()
        {
            if (currentQuestionCount < 10)
            {
                currentTrack = SongsToPlay[currentQuestionCount];
                music.Source = MusicService.Instance.mc.GetTrackSampleUri(SongsToPlay[currentQuestionCount].Id);
                music.Play();

                List<string> answers = new List<string>(3);
                answers = Shuffle<string>(new List<string> { currentTrack.GoodAnswer, currentTrack.BadAnswers[0], currentTrack.BadAnswers[1] });
                AnswerOne.Content = answers[0];
                AnswerTwo.Content = answers[1];
                AnswerThree.Content = answers[2];

                currentQuestionCount++;

                countText.Text = currentQuestionCount + "/" + 10;
            }
            else
            {
                NavigationService.Navigate(new Uri("/ResultPage.xaml?result=" + points.ToString(), UriKind.Relative));
            }
        }

        public void StartGame()
        {
            currentQuestionCount = 0;
            points = 0;
            SongsToPlay = InitSongsToPlayList();

            NextTrack();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode != System.Windows.Navigation.NavigationMode.Back || (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back && App.isNew))
            {
                App.isNew = false;
                await InitGame();
            }

            base.OnNavigatedTo(e);
        }

        private async void answer_Click(object sender, RoutedEventArgs e)
        {
            music.Stop();
            await CheckAnswer((sender as Button).Content as string);
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarAnswerButton =
                new ApplicationBarIconButton(new
                Uri("/Assets/AppBar/microphone.png", UriKind.Relative));
            appBarAnswerButton.Text = AppResources.VoiceAnswerButton;
            appBarAnswerButton.Click += VoiceAnswer_Click;

            ApplicationBar.Buttons.Add(appBarAnswerButton);
            ApplicationBar.BackgroundColor = Colors.White;
            ApplicationBar.ForegroundColor = Colors.Black;
            ApplicationBar.IsMenuEnabled = true;
            ApplicationBar.IsVisible = true;
        }

        private async Task CheckAnswer(string answer)
        {
            if (answer.Equals(currentTrack.GoodAnswer))
            {
                points += 100;
                answerPopupText.Text = "+100";

                answerPopupText.Foreground = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
            }
            else
            {
                points -= 50;
                answerPopupText.Text = "-50";

                answerPopupText.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            }

            answerPopup.IsOpen = true;
            await Task.Delay(500);
            answerPopup.IsOpen = false;

            Debug.WriteLine(points);
            NextTrack();
        }

        private List<T> Shuffle<T>(List<T> inputList)
        {
            List<T> randomList = new List<T>(inputList.Count);

            Random random = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = random.Next(0, inputList.Count);

                randomList.Add(inputList[randomIndex]);

                inputList.RemoveAt(randomIndex);
            }

            return randomList;
        }

        private async void VoiceAnswer_Click(object sender, EventArgs e)
        {
            music.Stop();

            // Create an instance of SpeechRecognizerUI.
            SpeechRecognizerUI recoWithUI = new SpeechRecognizerUI();
            //recoWithUI.Recognizer.SetRecognizer();
            if (recoWithUI != null)
            {

                recoWithUI.Settings.ReadoutEnabled = false;
                recoWithUI.Settings.ExampleText = AppResources.SpeechText;
                string[] answers = { currentTrack.GoodAnswer, currentTrack.BadAnswers[0], currentTrack.BadAnswers[1] };
                recoWithUI.Recognizer.Grammars.AddGrammarFromList("answer", answers);
                // Start recognition (load the dictation grammar by default).
                SpeechRecognitionUIResult recoResult = await recoWithUI.RecognizeWithUIAsync();

                if (recoResult.ResultStatus == SpeechRecognitionUIStatus.Succeeded)
                {
                    // Do something with the recognition result.
                    await CheckAnswer(recoResult.RecognitionResult.Text);
                }
            }
            else
            {
                MessageBox.Show(AppResources.RecognizerError);
            }
        }
    }
}
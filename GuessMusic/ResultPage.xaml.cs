﻿using GuessMusic.DataService;
using GuessMusic.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace GuessMusic
{
    public partial class ResultPage : PhoneApplicationPage
    {
        public ResultPage()
        {
            InitializeComponent();
        }

        public void UpdateTile()
        {
            ShellTile tile = ShellTile.ActiveTiles.FirstOrDefault();
            if (tile != null)
            {
                FlipTileData flipTile = new FlipTileData();

                flipTile.Title = AppResources.TileTitle;
                flipTile.BackTitle = AppResources.TileTitle;

                flipTile.BackContent = AppResources.LastScore + "\n" + ScoreText.Text;
                flipTile.BackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileMedium.png", UriKind.Relative);
                flipTile.BackBackgroundImage = new Uri("", UriKind.Relative);

                flipTile.WideBackgroundImage = new Uri("/Assets/Tiles/FlipCycleTileLarge.png", UriKind.Relative);
                flipTile.WideBackContent = AppResources.LastScore + "\n" + ScoreText.Text;
                flipTile.WideBackBackgroundImage = new Uri("", UriKind.Relative);

                tile.Update(flipTile);
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string result = string.Empty;
            if (NavigationContext.QueryString.TryGetValue("result", out result))
            {
                ScoreText.Text = result;
            }
            NavigationService.RemoveBackEntry();
            NavigationService.RemoveBackEntry();

            UpdateTile();

            string genreName = string.Empty;
            if (IsolatedStorageSettings.ApplicationSettings.Contains("lastGenre"))
            {
                genreName = IsolatedStorageSettings.ApplicationSettings["lastGenre"] as string;
            }

            int res = 0;
            int.TryParse(result, out res);

            await ScoreHistory.Instance.TryToAddScore(res, genreName);
        }
    }
}
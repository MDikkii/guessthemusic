﻿using GuessMusic.DataService;
using GuessMusic.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Nokia.Music.Tasks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace GuessMusic
{
    public partial class MainPage : PhoneApplicationPage
    {
        string AppCulture { get; set; }

        public MainPage()
        {
            InitializeComponent();
            
        }

        public void SetCulture()
        {
            if (CultureInfo.CurrentUICulture.Name.Equals("pl-PL") || CultureInfo.CurrentUICulture.Name.Equals("pl"))
                AppCulture = "pl-PL";
            else
                AppCulture = "en-US";

        }

        public void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        public void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/GenrePage.xaml", UriKind.Relative));
        }

        public void ScoreboardButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/ScoreboardPage.xaml", UriKind.Relative));
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            MessageBoxResult res = MessageBox.Show(AppResources.TryMixRadio, AppResources.BeforeExit, MessageBoxButton.OKCancel);

            if (res == MessageBoxResult.OK)
            {
                LaunchTask task = new LaunchTask();
                task.Show();
            }
            if (res == MessageBoxResult.Cancel)
            {
                e.Cancel = false;
            }
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await MusicService.Instance.InitGenres();
            base.OnNavigatedTo(e);
        }
    }
}
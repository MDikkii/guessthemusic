﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessMusic.DataService
{
    public class TrackQuestion
    {
        public TrackQuestion(string name, string id, string artist, string goodAnswer, string[] badAnswers)
        {
            Name = name;
            Id = id;
            Artist = artist;
            GoodAnswer = goodAnswer;
            BadAnswers = badAnswers;
        }

        public string Artist { get; set; }

        public string[] BadAnswers { get; set; }

        public string GoodAnswer { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }
    }
}
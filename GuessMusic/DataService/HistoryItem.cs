﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessMusic.DataService
{
    public class HistoryItem
    {
        public int FirstPlace { get; set; }

        public string GenreName { get; set; }

        public int SecondPlace { get; set; }

        public int ThirdPlace { get; set; }
    }
}
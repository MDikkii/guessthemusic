﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessMusic.DataService
{
    public class GenreHelp
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
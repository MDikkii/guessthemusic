﻿using GuessMusic.Resources;
using Nokia.Music;
using Nokia.Music.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GuessMusic.DataService
{
    public sealed class MusicService
    {
        private static readonly MusicService msInstance = new MusicService();

        static MusicService()
        {
        }

        private MusicService()
        {
            mc = new MusicClient("fadc7514fbe6d398cb2e3ed7ab0c2194");
            Debug.WriteLine("MusicServiceCreate");
        }

        public static MusicService Instance
        {
            get
            {
                return msInstance;
            }
        }

        public MusicClient mc { get; set; }

        private List<GenreHelp> Genres { get; set; }

        public async Task<List<GenreHelp>> GetGenres()
        {
            if (Genres == null)
            {
                await InitGenres();
            }
            return Genres;
        }

        public async Task InitGenres()
        {
            try
            {
                if (Genres == null)
                {
                    var gen = await mc.GetGenresAsync();

                    Genres = new List<GenreHelp>();

                    foreach (Nokia.Music.Types.Genre g in gen)
                    {
                        Debug.WriteLine(g.Name + " " + g.Id);
                        Genres.Add(new GenreHelp() { Name = g.Name, Id = g.Id });
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(AppResources.LackOfInternet);
                Genres = null;
            }
        }
    }
}
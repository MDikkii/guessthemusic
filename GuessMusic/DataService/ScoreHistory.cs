﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;

namespace GuessMusic.DataService
{
    public class ScoreHistory
    {
        private static readonly ScoreHistory msInstance = new ScoreHistory();

        private List<HistoryItem> History;

        static ScoreHistory()
        {
        }

        private ScoreHistory()
        {
            History = new List<HistoryItem>();
        }

        public static ScoreHistory Instance
        {
            get
            {
                return msInstance;
            }
        }

        public async Task ClearHistory()
        {
            History.Clear();
            await WriteToXml();
        }

        public List<HistoryItem> GetHistory()
        {
            return History;
        }

        public async Task InitHistory()
        {
            await ReadFromXml();
        }

        public async Task ReadFromXml()
        {
            Debug.WriteLine("Read Serwis");

            await Task.Run(() =>
            {
                try
                {
                    using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("history.xml", FileMode.Open))
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<HistoryItem>));
                            History = (List<HistoryItem>)serializer.Deserialize(stream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //If first run there will be FileNotFoundException
                    if (ex is FileNotFoundException)
                    {
                    }
                    else
                    {
                        //TO DO
                    }
                }
            });
        }

        public async Task TryToAddScore(int score, string genreName)
        {
            if (History.Exists(x => x.GenreName == genreName))
            {
                var ind = History.FindIndex(x => x.GenreName == genreName);
                var hi = History.ElementAt(ind);
                if (hi.FirstPlace < score)
                {
                    hi.ThirdPlace = hi.SecondPlace;
                    hi.SecondPlace = hi.FirstPlace;
                    hi.FirstPlace = score;
                }
                else
                    if (hi.SecondPlace < score)
                    {
                        hi.ThirdPlace = hi.SecondPlace;
                        hi.SecondPlace = score;
                    }
                    else
                        if (hi.ThirdPlace < score)
                        {
                            hi.ThirdPlace = score;
                        }
            }
            else
            {
                History.Add(new HistoryItem() { GenreName = genreName, FirstPlace = score, SecondPlace = 0, ThirdPlace = 0 });
            }
            await WriteToXml();
        }

        public async Task WriteToXml()
        {
            Debug.WriteLine("Write Serwis");
            await Task.Run(() =>
            {
                XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                xmlWriterSettings.Indent = true;

                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("history.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<HistoryItem>));
                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, History);
                        }
                    }
                }
            });
        }
    }
}